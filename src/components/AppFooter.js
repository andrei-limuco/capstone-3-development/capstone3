import { useContext } from "react";
import { Container, Row, Col } from "react-bootstrap";
// import { Link } from "react-router-dom";
import * as BsIcons from "react-icons/bs";
import UserContext from "../pages/UserContext";

export default function Footer() {
	const { user } = useContext(UserContext);

	const currentYear = new Date().getFullYear();

	return !user.isAdmin ? (
		<footer id="footer" className="footerColor">
			<Container fluid className="padding-x pt-5 pb-4 text-white">
				<Row className="row justify-content-between px-lg-3">
					<Col s={7}>
						<div className="d-flex justify-content-between justify-content-md-start mb-5 mb-md-0">
							<div className="me-5">
								<nav>
									<ul className="flex-column">
										<li className="mb-2 text-nowrap">
											{/* <Link className="text-white" to="/shop-all">
												SHOP ALL
											</Link> */}
											SHOP ALL
										</li>
										<li className="mb-2 text-nowrap">
											{/* <a className="text-white" href="/#trending">
												TRENDING
											</a> */}
											TRENDING
										</li>
										<li className="mb-2 text-nowrap">
											{/* <a className="text-white" href="/#featured-collections">
												FEATURED
											</a> */}
											FEATURED
										</li>
										<li className="mb-2 text-nowrap">
											{/* <a className="text-white" href="/#best-sellers">
												BEST SELLERS
											</a> */}
											BEST SELLERS
										</li>
									</ul>
								</nav>
							</div>
							<div className="me-5">
								<nav>
									<ul className="flex-column">
										<a
											href="https://andreilimuco.github.io/webportfolio"
											target="_blank"
										>
											<li className="mb-2 text-nowrap text-white">
												VIEW PORTFOLIO
											</li>
										</a>
										<a
											href="https://www.linkedin.com/in/lemuel-andrei-limuco/"
											target="_blank"
										>
											<li className="mb-2 text-nowrap text-white">LINKEDIN</li>
										</a>
										<a href="https://github.com/andreilimuco" target="_blank">
											<li className="mb-2 text-nowrap text-white">GITHUB</li>
										</a>
									</ul>
								</nav>
							</div>
						</div>
					</Col>
					<Col
						s={5}
						className="d-flex justify-content-start justify-content-md-end"
					>
						<i className="pe-3">
							<a
								className="text-white"
								href="https://www.facebook.com/andrei.limuco"
								target="_blank"
							>
								<BsIcons.BsFacebook size={20} />
							</a>
						</i>
						<i className="pe-3">
							<a
								className="text-white"
								href="https://www.twitter.com/la_limuco"
								target="_blank"
							>
								<BsIcons.BsTwitter size={20} />
							</a>
						</i>
						<i className="">
							<a
								className="text-white"
								href="https://www.instagram.com/la_limuco"
								target="_blank"
							>
								<BsIcons.BsInstagram size={20} />
							</a>
						</i>
					</Col>
				</Row>

				<Row className="px-lg-3 mt-5">
					<Col>
						<h3 className="big-heading text-nowrap me-5">THE DRIP</h3>
					</Col>
					<Col className="d-flex justify-content-start justify-content-md-end">
						<p className="copyright text-nowrap">
							© {currentYear} The Drip, Inc. All rights reserved.
						</p>
					</Col>
				</Row>
			</Container>
		</footer>
	) : null;
}
