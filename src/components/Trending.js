import { useState, useEffect } from "react";
import ProductCard from "./ProductCard";
import { Container, Row, Col } from "react-bootstrap";
// import Skeleton from "react-loading-skeleton";
import SkeletonLoader from "./SkeletonLoader";

export default function Trending() {
	const [trending, setTrending] = useState([]);
	const [isLoading, setIsLoading] = useState(true);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/trending`)
			.then((res) => res.json())
			.then((data) => {
				setTrending(
					data.map((trendingProduct) => (
						<Col xs={6} lg={3} className="mb-2" key={trendingProduct._id}>
							<ProductCard
								id={trendingProduct._id}
								name={trendingProduct.name}
								price={trendingProduct.price}
								src={trendingProduct.src}
							/>
						</Col>
					))
				);
				setIsLoading(false);
			})
			.catch((error) => {
				console.error("Error fetching products:", error);
				setIsLoading(false);
			});
	}, []);

	// Render 4 skeleton placeholders while loading
	// const skeletons = Array(4).fill(
	// 	<Col xs={6} lg={3} className="mb-2">
	// 		<Skeleton height={200} />
	// 		<Skeleton height={20} width={`60%`} />
	// 		<Skeleton height={20} width={`40%`} />
	// 	</Col>
	// );

	return (
		<section id="trending">
			<Container fluid className="padding-x pt-5 pb-4 pb-md-0 pb-lg-3">
				<div className="p-12px">
					<h1 className="big-heading mb-3">TRENDING</h1>
				</div>
				<Row className="px-lg-3">
				{isLoading ? <SkeletonLoader /> : trending}
				</Row>
			</Container>
		</section>
	);
}
