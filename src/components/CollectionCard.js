import { Link } from "react-router-dom";

export default function CollectionCard(props) {
	return (
		<div className="mb-5 mb-md-0">
			<Link to={props.destination}>
				<img
					src={props.img.src}
					className="card-img-top rounded-0"
					alt={props.img.alt}
				/>
			</Link>
			<h3 className="mt-2">{props.title}</h3>
		</div>
	);
}
