import { Link } from "react-router-dom";
export default function ProductCard(props) {
	return (
		<div>
			<Link to={`/products/${props.id}`}>
				<img variant="top" src={props.src} alt="" className="w-100" />
			</Link>
			<div>
				<div className="mt-2 ms-2 ms-md-0">
					<p className="mb-2">{props.name}</p>
					<p className="mb-2 text-secondary">₱{props.price.toLocaleString()}</p>
				</div>
			</div>
		</div>
	);
}
