import { useState, useEffect } from "react";
import ProductCard from "./ProductCard";
import { Container, Row, Col } from "react-bootstrap";
import SkeletonLoader from "./SkeletonLoader";

export default function BestSellers() {
	const [bestSellers, setBestSellers] = useState([]);
	const [isLoading, setIsLoading] = useState(true)

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/bestSellers`)
			.then((res) => res.json())
			.then((data) => {
				//console.log(data)
				setBestSellers(
					data.map((bestSellerProduct) => {
						return (
							<Col xs={6} lg={3} className="mb-2" key={bestSellerProduct._id}>
								<ProductCard
									id={bestSellerProduct._id}
									name={bestSellerProduct.name}
									price={bestSellerProduct.price}
									src={bestSellerProduct.src}
								/>
							</Col>
						);
					})
				);
				setIsLoading(false);
			})
			.catch((error) => {
				console.error("Error fetching products:", error);
				setIsLoading(false);
			});
	}, []);

	return (
		<section id="best-sellers">
			<Container fluid className="padding-x pb-5 pt-lg-4 mb-lg-4">
				<div className="p-12px">
					<h1 className="big-heading mb-3">BEST SELLERS</h1>
				</div>
				<Row className="px-lg-3">{isLoading ? <SkeletonLoader /> : bestSellers}</Row>
			</Container>
		</section>
	);
}
