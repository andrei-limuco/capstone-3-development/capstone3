import { Link } from "react-router-dom";

import * as BsIcons from "react-icons/bs";
import Badge from "@material-ui/core/Badge";

import * as GOIcons from "react-icons/go";

import {
	Navbar,
	Container,
	Offcanvas,
	Nav,
	NavDropdown,
} from "react-bootstrap";

import { useContext, Fragment } from "react";
import UserContext from "../pages/UserContext";

export default function AppNavbar() {
	const { user, badgeCount } = useContext(UserContext);

	return (
		<Navbar
			collapseOnSelect
			className="fixed-top padding-x navbar-dark"
			bg="black"
			expand={false}
		>
			<Container fluid>
				<Navbar.Brand>
					<Nav.Link
						href="/"
						className={
							user.id === null
								? "big-heading text-white me-5"
								: "big-heading text-white"
						}
						id="navbar-brand"
					>
						THE DRIP
					</Nav.Link>
				</Navbar.Brand>

				{user.isAdmin ? (
					<div className="d-none d-lg-block d-lg-flex justify-content-center align-items-center">
						<Nav.Link as={Link} to="/shop-all" className="mx-4 text-white">
							VIEW PRODUCTS
						</Nav.Link>

						{/* <Nav.Link as={Link} to="/" className="mx-4 text-white">
							USERS
						</Nav.Link> */}
						<Nav.Link as={Link} to="/admin/orders" className="mx-4 text-white">
							VIEW ORDERS
						</Nav.Link>
					</div>
				) : (
					<div className="d-none d-lg-block d-lg-flex justify-content-center align-items-center">
						<Nav.Link as={Link} to="/shop-all" className="mx-4 text-white">
							SHOP ALL
						</Nav.Link>
						<a className="mx-4 text-white" href="/#trending">
							TRENDING
						</a>
						<a className="mx-4 text-white" href="/#featured-collections">
							FEATURED
						</a>
						<a className="mx-4 text-white" href="/#best-sellers">
							BEST SELLERS
						</a>
					</div>
				)}

				<div className="d-flex align-items-center">
					{user.isAdmin || user.id === null ? null : (
						<Link to="/cart">
							<Badge color="secondary" badgeContent={badgeCount}>
								<BsIcons.BsCart4
									size={25}
									className="text-white  small-margin-bottom"
								/>
							</Badge>
						</Link>
					)}

					<Navbar.Toggle
						className="d-flex d-lg-none ms-2 ms-lg-3"
						aria-controls="offcanvasNavbar"
					/>
					{user.id !== null ? (
						<NavDropdown
							align="end"
							title={
								<span className="text-white ms-2 ms-lg-3">
									Hi, {user.firstName}
								</span>
							}
							id="basic-nav-dropdown align-end"
							className="navDropdown d-none d-lg-block "
						>
							{user.isAdmin ? null : (
								<NavDropdown.Item as={Link} to="/orders">
									VIEW ORDERS
								</NavDropdown.Item>
							)}

							<NavDropdown.Item as={Link} to="/logout">
								LOG OUT
							</NavDropdown.Item>
						</NavDropdown>
					) : (
						<div className="d-flex justify-content-between align-items-center">
							<span>
								<Link to="/login" className="text-white d-none d-lg-block">
									LOG IN
								</Link>
							</span>
							<span className="mx-3 text-white small-padding-bottom d-none d-lg-block">
								|
							</span>
							<span>
								<Link to="/signup" className="text-white d-none d-lg-block">
									REGISTER
								</Link>
							</span>
						</div>
					)}
				</div>

				<Navbar.Offcanvas
					id="offcanvasNavbar"
					aria-labelledby="offcanvasNavbarLabel"
					placement="end"
				>
					<Offcanvas.Header closeButton className="border-bottom">
						<Offcanvas.Title id="offcanvasNavbarLabel" className="big-heading">
							THE DRIP
						</Offcanvas.Title>
					</Offcanvas.Header>
					<Offcanvas.Body>
						<Nav className="justify-content-end flex-grow-1 pe-3">
							{user.isAdmin ? (
								<Fragment>
									<Nav.Link eventKey="1" as={Link} to="/" className="text-dark">
										UPDATE PRODUCTS
									</Nav.Link>
									<Nav.Link
										eventKey="1"
										as={Link}
										to="/shop-all"
										className="text-dark"
									>
										VIEW PRODUCTS
									</Nav.Link>

									{/* <Nav.Link
								eventKey="1"
								as={Link}
								to="/air-jordan-1"
								className="text-dark"
							>
								USERS
							</Nav.Link> */}
									<Nav.Link
										eventKey="1"
										as={Link}
										to="/admin/orders"
										className="text-dark"
									>
										VIEW ORDERS
									</Nav.Link>
								</Fragment>
							) : (
								<Fragment>
									<Nav.Link
										eventKey="1"
										as={Link}
										to="/shop-all"
										className="text-dark"
									>
										SHOP ALL
									</Nav.Link>
									<Nav.Link
										eventKey="1"
										as={Link}
										to="/air-jordan-1"
										className="text-dark"
									>
										AIR JORDAN 1
									</Nav.Link>
									<Nav.Link
										eventKey="1"
										as={Link}
										to="/nike-air-force-1"
										className="text-dark"
									>
										NIKE AIR FORCE 1
									</Nav.Link>
									<Nav.Link
										eventKey="1"
										as={Link}
										to="/nike-dunk"
										className="text-dark"
									>
										NIKE DUNK
									</Nav.Link>
								</Fragment>
							)}

							<div className="border-top my-4"></div>
							{!user.isAdmin && user.id !== null ? (
								<Fragment>
									<Nav.Link
										eventKey="1"
										as={Link}
										to="/cart"
										className="link-dark text-decoration-none d-flex align-items-center"
									>
										<Badge color="secondary" badgeContent={badgeCount}>
											<BsIcons.BsCart4
												size={25}
												className="text-dark small-margin-bottom"
											/>
										</Badge>
										<span className="ms-3">CART</span>
									</Nav.Link>
									<Nav.Link
										eventKey="1"
										as={Link}
										to="/orders"
										className="link-dark text-decoration-none d-flex align-items-center"
									>
										<GOIcons.GoPackage
											size={25}
											className="text-dark small-margin-bottom"
										/>

										<span className="ms-3">ORDERS</span>
									</Nav.Link>
									<div className="border-top my-4"></div>
								</Fragment>
							) : null}

							{user.id !== null ? (
								<Fragment>
									<Nav.Link eventKey="1" as={Link} to="/">
										<span className="text-dark">Hi, {user.firstName}</span>
									</Nav.Link>
									<Nav.Link
										eventKey="1"
										as={Link}
										to="/logout"
										className="text-dark"
									>
										LOG OUT
									</Nav.Link>
								</Fragment>
							) : (
								<Fragment>
									<Nav.Link
										eventKey="1"
										as={Link}
										to="/login"
										className="text-dark"
									>
										LOGIN
									</Nav.Link>
									<Nav.Link
										eventKey="1"
										as={Link}
										to="/signup"
										className="text-dark"
									>
										REGISTER
									</Nav.Link>
								</Fragment>
							)}
						</Nav>
					</Offcanvas.Body>
				</Navbar.Offcanvas>
			</Container>
		</Navbar>
	);
}
