import CollectionCard from "./CollectionCard";
import { Container, Row, Col } from "react-bootstrap";

export default function Collections() {
	const collections = [
		{
			destination: "/air-jordan-1",
			img: {
				src: "images/featured-1.png",
				alt: "featured-1-image",
			},
			title: "Air Jordan 1",
		},
		{
			destination: "/nike-air-force-1",
			img: {
				src: "images/featured-2.jpg",
				alt: "featured-2-image",
			},
			title: "Nike Air Force 1",
		},
		{
			destination: "/nike-dunk",
			img: {
				src: "images/featured-3.jpg",
				alt: "featured-3-image",
			},
			title: "Nike Dunk",
		},
	];

	return (
		<section id="featured-collections">
			<Container fluid className="padding-x py-md-5">
				<div className="p-12px">
					<h1 className="big-heading mb-3">FEATURED</h1>
				</div>
				<Row className="px-lg-3">
					{collections.map((collection, index) => {
						return (
							<Col md={4} key={index}>
								<CollectionCard
									destination={collection.destination}
									img={collection.img}
									title={collection.title}
								/>
							</Col>
						);
					})}
				</Row>
			</Container>
		</section>
	);
}
