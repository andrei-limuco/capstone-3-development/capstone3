import React from "react";
import { Col } from "react-bootstrap";
import Skeleton from "react-loading-skeleton";
import 'react-loading-skeleton/dist/skeleton.css'; // Import CSS for styling

export default function SkeletonLoader({ items = 4, height = 200 }) {
  // Generate an array of skeletons based on the 'items' prop
  const skeletonItems = Array.from({ length: items }).map((_, index) => (
    <Col xs={6} lg={3} className="mb-2" key={index}>
      <Skeleton className="skeleton-loader" height={height} />
      <Skeleton height={20} width={`60%`} style={{ marginTop: 10 }} />
      <Skeleton height={20} width={`40%`} />
    </Col>
  ));

  return <>{skeletonItems}</>;
}
