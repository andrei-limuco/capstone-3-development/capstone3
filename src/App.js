import { BrowserRouter as Router } from "react-router-dom";
import { UserProvider } from "./pages/UserContext";
import { useEffect, useState } from "react";
import { Routes, Route } from "react-router-dom";
import AppNavbar from "./components/AppNavbar";
import AppFooter from "./components/AppFooter";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Signup from "./pages/Signup";
import ShopAll from "./pages/ShopAll";
import ProductView from "./pages/ProductView";
import CartView from "./pages/CartView";
import OrderView from "./pages/OrderView";
import AirJordan1 from "./pages/AirJordan1";
import NikeAirForce1 from "./pages/NikeAirForce1";
import NikeDunk from "./pages/NikeDunk";
import Error from "./pages/Error";
import AdminOrdersView from "./pages/AdminOrdersView";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

import PlaceholderTest from "./pages/Placeholder";

export default function App() {
	const [badgeCount, setBadgeCount] = useState(0);
	let badge = [];

	const [totalAmount, setTotalAmount] = useState(0);

	const [user, setUser] = useState({
		id: null,
		isAdmin: null,
		firstName: null,
	});

	const unsetUser = () => {
		localStorage.clear();
		// setBadgeCount(0);
	};

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				// console.log(data);
				if (typeof data._id !== "undefined") {
					setUser({
						id: data._id,
						isAdmin: data.isAdmin,
						firstName: data.firstName,
					});
				} else {
					setUser({
						id: null,
						isAdmin: null,
						firstName: null,
					});
				}
			});
	}, [user.firstName]);

	const getUserCart = () => {
		fetch(`${process.env.REACT_APP_API_URL}/carts`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				setBadgeCount(data.length);
			});
	};

	useEffect(() => {
		getUserCart();
	}, [badge]);

	return (
		<UserProvider
			value={{
				user,
				setUser,
				unsetUser,
				badgeCount,
				setBadgeCount,
				totalAmount,
				setTotalAmount,
			}}
		>
			<Router>
				<AppNavbar />
				<Routes>
					<Route exact path="/" element={<Home />} />
					<Route exact path="/login" element={<Login />} />
					<Route exact path="/logout" element={<Logout />} />
					<Route exact path="/signup" element={<Signup />} />
					<Route exact path="/shop-all" element={<ShopAll />} />
					<Route exact path="/products/:productId" element={<ProductView />} />
					<Route exact path="/cart" element={<CartView />} />
					<Route exact path="/orders" element={<OrderView />} />
					<Route exact path="/air-jordan-1" element={<AirJordan1 />} />
					<Route exact path="/nike-air-force-1" element={<NikeAirForce1 />} />
					<Route exact path="/nike-dunk" element={<NikeDunk />} />
					<Route exact path="/placeholder" element={<PlaceholderTest />} />
					<Route exact path="/admin/orders" element={<AdminOrdersView />} />
					<Route path="*" element={<Error />} />
				</Routes>
				<AppFooter />
			</Router>
		</UserProvider>
	);
}
