import { useState, useEffect, useContext } from "react";
import { Link, Navigate } from "react-router-dom";
import UserContext from "./UserContext";
import { Button } from "react-bootstrap";
import Swal from "sweetalert2";

export default function Login() {
	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState("");

	function loginUser(e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email: email,
				password: password,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (typeof data.access !== "undefined") {
					localStorage.setItem("token", data.access);
					retrieveUserDetails(data.access);

					Swal.fire({
						title: "Login Successful",
						icon: "success",
					});

					setEmail("");
					setPassword("");
				} else {
					Swal.fire({
						title: "Authentication Failed",
						icon: "error",
						text: "Check your login details and try again!",
					});
				}
			});
	}

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				//console.log(data)
				//console.log(data._id)
				//console.log(data.isAdmin)

				setUser({
					id: data._id,
					isAdmin: data.isAdmin,
				});
			});
	};

	useEffect(() => {
		if (email !== "" && password !== "") {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password]);

	return user.id !== null ? (
		<Navigate to="/" />
	) : (
		<main className="min-vh-100 form-signin text-center my-5 my-md-0">
			<form onSubmit={(e) => loginUser(e)}>
				{/* <img
					className="mb-4"
					src="/images/logo.png"
					alt=""
					width="72"
					height="72"
				/> */}
				<h1 className=" mb-3 fw-normal big-heading">LOGIN</h1>

				<div className="form-floating">
					<input
						onChange={(e) => setEmail(e.target.value)}
						value={email}
						type="email"
						className="form-control"
						id="floatingInput"
						placeholder="name@example.com"
					/>
					<label htmlFor="floatingInput">Email address</label>
				</div>
				<div className="form-floating">
					<input
						onChange={(e) => setPassword(e.target.value)}
						value={password}
						type="password"
						className="form-control"
						id="floatingPassword"
						placeholder="Password"
					/>
					<label htmlFor="floatingPassword">Password</label>
				</div>

				<div className="checkbox mb-3"></div>

				{isActive ? (
					<Button
						variant="dark"
						className="w-100 btn btn-lg rounded-0"
						type="submit"
					>
						SIGN IN
					</Button>
				) : (
					<Button
						variant="dark"
						className="w-100 btn btn-lg rounded-0 disabled"
						type="submit"
					>
						SIGN IN
					</Button>
				)}

				<p className="mt-5 mb-3 text-muted">
					Not yet registered?{" "}
					<Link to="/signup" className="link-dark">
						Create Account
					</Link>
				</p>
			</form>
		</main>
	);
}
