import { useEffect, useState, useContext, Fragment } from "react";
import { Container, Button, Table, Modal, Form } from "react-bootstrap";
import { Navigate, Link, useNavigate } from "react-router-dom";
import UserContext from "./UserContext";
import Swal from "sweetalert2";

export default function AdminDashboard() {
	const { user } = useContext(UserContext);
	const [products, setProducts] = useState();
	const navigate = useNavigate();
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
	const [id, setId] = useState();
	const [category, setCategory] = useState("");
	const [name, setName] = useState();
	const [description, setDescription] = useState();
	const [price, setPrice] = useState();
	const [src, setSrc] = useState();
	const [mode, setMode] = useState();

	// Add product
	function addProduct(e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				category: category,
				src: src,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data === true) {
					Swal.fire({
						title: "Product Added",
						icon: "success",
					});
				}

				handleClose();
				setMode("");
				setName("");
				setCategory("");
				setDescription("");
				setPrice("");
				setSrc("");
			});
	}

	// Update product
	function updateProduct(e) {
		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				category: category,
				src: src,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				console.log(data);
				Swal.fire({
					title: "Update Successful!",
					icon: "success",
				});
				handleClose();
				setId("");
				setMode("");
				setName("");
				setDescription("");
				setPrice("");
				setCategory("");
				setSrc("");
			});
	}

	// Archive product
	function archiveProduct(productId, productName) {
		fetch(
			`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${localStorage.getItem("token")}`,
				},
			}
		)
			.then((res) => res.json())
			.then((data) => {
				if (data === true) {
					Swal.fire({
						title: ` Archived Succesfully`,
						icon: "success",
					});
				}
			});
	}

	// Unarchive product
	function unarchiveProduct(productId, productName) {
		fetch(
			`${process.env.REACT_APP_API_URL}/products/${productId}/unarchive`,
			{
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${localStorage.getItem("token")}`,
				},
			}
		)
			.then((res) => res.json())
			.then((data) => {
				if (data === true) {
					Swal.fire({
						title: `Product Activated`,
						icon: "success",
					});
				}
			});
	}

	useEffect(() => {
		if (user.isAdmin) {
			fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${localStorage.getItem("token")}`,
				},
			})
				.then((res) => res.json())
				.then((data) => {
					setProducts(
						data.reverse().map((product) => {
							return product.isActive ? (
								<tr key={product._id}>
									<td>{product.category}</td>
									<td>{product.name}</td>
									<td>{product.src}</td>
									<td>{product.description}</td>
									<td>₱{product.price.toLocaleString()}</td>
									<td>Available</td>
									<td>
										<div className="d-flex align-items-center">
											<button
												className="btn btn-outline-dark rounded-0 me-2"
												onClick={() => {
													handleShow();
													setMode("update");
													setId(product._id);
													setName(product.name);
													setDescription(product.description);
													setPrice(product.price);
												}}
											>
												Update
											</button>
											<button
												className="btn btn-danger rounded-0"
												onClick={(e) =>
													archiveProduct(product._id, product.name)
												}
											>
												Archive
											</button>
										</div>
									</td>
								</tr>
							) : (
								<tr key={product._id}>
									<td>{product.category}</td>
									<td>{product.name}</td>
									<td>{product.src}</td>
									<td>{product.description}</td>
									<td>₱{product.price}</td>
									<td>Unavailable</td>
									<td>
										<div className="d-flex align-items-center">
											<button
												className="btn btn-outline-dark rounded-0 me-2"
												onClick={() => {
													handleShow();
													setMode("update");
													setId(product._id);
													setName(product.name);
													setDescription(product.description);
													setPrice(product.price);
												}}
											>
												Update
											</button>
											<button
												className="btn btn-success rounded-0"
												onClick={(e) =>
													unarchiveProduct(product._id, product.name)
												}
											>
												Unarchive
											</button>
										</div>
									</td>
								</tr>
							);
						})
					);
				});
		}
	}, [user, products]);

	return user.id !== null || localStorage.getItem("token") !== null ? (
		user.isAdmin ? (
			<Fragment>
				<Container fluid className="min-vh-100 padding-x">
					<div className="p-15px">
						<h1 className="mt-5 text-center big-heading">Admin Dashboard</h1>
						<div className="text-center my-4">
							<Button
								className="btn btn-dark rounded-0"
								variant="dark"
								onClick={() => {
									handleShow();
									setMode("add");
								}}
							>
								Add New Product
							</Button>
						</div>
						<Table responsive striped bordered hover className="mt-5 mb-5">
							<thead>
								<tr>
									<th>Category</th>
									<th>Name</th>
									<th>Image src</th>
									<th>Description</th>
									<th>Price</th>
									<th>Availability</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>{products}</tbody>
						</Table>
					</div>
				</Container>
				{/* MODAL */}
				<Modal centered show={show} onHide={handleClose}>
					<Modal.Header closeButton>
						{mode === "add" ? (
							<Modal.Title>Add New Product</Modal.Title>
						) : (
							<Modal.Title>Update Product</Modal.Title>
						)}
					</Modal.Header>
					<Form
						onSubmit={(e) => {
							if (mode === "add") {
								addProduct(e);
							} else {
								updateProduct(e);
							}
						}}
					>
						<Modal.Body>
							{mode === "update" ? (
								<Form.Control
									type="text"
									value={id}
									onChange={(e) => setId(e.target.value)}
									hidden
								/>
							) : null}
							<Form.Group className="mb-3" controlId="category">
								<Form.Label>Category</Form.Label>
								<Form.Control
									type="text"
									placeholder="Air Jordan 1"
									value={category}
									onChange={(e) => setCategory(e.target.value)}
									required
								/>
							</Form.Group>

							<Form.Group className="mb-3" controlId="productName">
								<Form.Label>Name</Form.Label>
								<Form.Control
									type="text"
									placeholder="Air Jordan 1 Low"
									value={name}
									onChange={(e) => setName(e.target.value)}
									required
								/>
							</Form.Group>

							<Form.Group className="mb-3" controlId="src">
								<Form.Label>Image src</Form.Label>
								<Form.Control
									type="text"
									placeholder="/images/black-toe.png"
									value={src}
									onChange={(e) => setSrc(e.target.value)}
									required
								/>
							</Form.Group>

							<Form.Group className="mb-3" controlId="productDescription">
								<Form.Label>Description</Form.Label>
								<Form.Control
									as="textarea"
									placeholder="Fire Sneaker"
									value={description}
									onChange={(e) => setDescription(e.target.value)}
									required
								/>
							</Form.Group>

							<Form.Group className="mb-3" controlId="productPrice">
								<Form.Label>Price</Form.Label>
								<Form.Control
									type="number"
									placeholder="₱99999"
									value={price}
									onChange={(e) => setPrice(e.target.value)}
									required
								/>
							</Form.Group>
						</Modal.Body>
						<Modal.Footer>
							<Button
								className="rounded-0"
								variant="danger"
								onClick={handleClose}
							>
								Close
							</Button>
							<Button
								variant="dark"
								type="submit"
								className="submitBtn rounded-0"
							>
								{mode === "add" ? "Add New Product" : "Update Product"}
							</Button>
						</Modal.Footer>
					</Form>
				</Modal>
			</Fragment>
		) : (
			<Navigate to="/" />
		)
	) : (
		<Navigate to="/login" />
	);
}
