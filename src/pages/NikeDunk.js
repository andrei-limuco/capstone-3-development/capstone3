import { Fragment, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Breadcrumb, Container, Col, Row } from "react-bootstrap";
import ProductCard from "../components/ProductCard";
import Swal from "sweetalert2";

export default function AirJordan1() {
	const [dunks, setDunks] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	const [initialRun, setInitialRun] = useState(true);

	const timer = () => {
		let timerInterval;
		Swal.fire({
			title: "Loading...",
			timer: 700,
			timerProgressBar: true,
			didOpen: () => {
				Swal.showLoading();
			},
			willClose: () => {
				clearInterval(timerInterval);
			},
		});
	};

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/nikeDunk`)
			.then((res) => res.json())
			.then((data) => {
				//console.log(data)
				setDunks(
					data.map((dunk) => {
						return (
							<Col xs={6} lg={3} className="mb-2 mb-lg-4" key={dunk._id}>
								<ProductCard
									id={dunk._id}
									name={dunk.name}
									price={dunk.price}
									src={dunk.src}
								/>
							</Col>
						);
					})
				);
				setIsLoading(false);
			});
		if (initialRun) {
			setInitialRun(false);
			timer();
		}
	}, [initialRun]);

	return (
		<Fragment>
			<section id="nike-dunk">
				<Container fluid className="min-vh-100 padding-x py-5">
					<Breadcrumb className="px-lg-3">
						<Breadcrumb.Item>
							<Link to="/" className="text-dark">
								HOME
							</Link>
						</Breadcrumb.Item>
						<Breadcrumb.Item active>NIKE DUNK</Breadcrumb.Item>
					</Breadcrumb>
					<h1 className="big-heading px-lg-3 mt-4 mb-5">NIKE DUNK</h1>
					{isLoading ? (
						<p className="ms-lg-3">Loading...</p>
					) : (
						<Row className="px-lg-3">{dunks}</Row>
					)}
				</Container>
			</section>
		</Fragment>
	);
}
