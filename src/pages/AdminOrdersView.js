import { useState, useEffect, useContext } from "react";
import { Container, Table } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import UserContext from "./UserContext";

export default function AdminOrdersView() {
	const { user } = useContext(UserContext);
	const [orders, setOrders] = useState();

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/orders`, {
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				// console.log(data);
				// console.log(data[0].items[0].product[0].name);

				// data.map((order) => {
				// 	console.log(order.items[0].product[0].name);
				// });

				setOrders(
					data.reverse().map((order) => {
						let orderDate = new Date(order.purchasedOn).toLocaleDateString();
						return (
							<tr key={order._id}>
								<td>{order._id}</td>
								<td>
									{order.items.map((subItem) => {
										return (
											<ol>
												{" "}
												<li key={subItem._id}>
													{subItem.product[0].name}
												</li>{" "}
											</ol>
										);
									})}
								</td>
								<td>
									{order.items.map((subItem) => {
										return (
											<ol>
												<li key={subItem._id}>
													₱{subItem.product[0].price.toLocaleString()}
												</li>
											</ol>
										);
									})}
								</td>
								<td>
									{order.items.map((subItem) => {
										return (
											<ol>
												<li key={subItem._id}>{subItem.product[0].quantity}</li>
											</ol>
										);
									})}
								</td>
								<td>₱{order.totalAmount.toLocaleString()}</td>
								<td>{order.userId}</td>
								<td>{orderDate}</td>
							</tr>
						);
					})
				);
			});
	}, [orders]);

	return user.id !== null || localStorage.getItem("token") !== null ? (
		user.isAdmin ? (
			<Container fluid className="min-vh-100 padding-x">
				<div className="p-15px">
					<h1 className="mt-5 text-center big-heading">All User Orders</h1>
					<Table responsive bordered striped hover className="mt-5 mb-5">
						<thead>
							<tr>
								<th>Order Number</th>
								<th>Product Name</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Total Amount</th>
								<th>User ID</th>
								<th>Purchased On</th>
							</tr>
						</thead>
						<tbody>{orders}</tbody>
					</Table>
				</div>
			</Container>
		) : (
			<Navigate to="/" />
		)
	) : (
		<Navigate to="/login" />
	);
}
