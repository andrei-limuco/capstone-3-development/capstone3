import { Fragment, useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { Breadcrumb, Container, Col, Row } from "react-bootstrap";
import ProductCard from "../components/ProductCard";
import Swal from "sweetalert2";
import UserContext from "./UserContext";

export default function ShopAll() {
	const { user } = useContext(UserContext);
	const [products, setProducts] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	const [initialRun, setInitialRun] = useState(true);

	// const timer = () => {
	// 	let timerInterval;
	// 	Swal.fire({
	// 		title: "Loading...",
	// 		timer: 700,
	// 		timerProgressBar: true,
	// 		didOpen: () => {
	// 			Swal.showLoading();
	// 		},
	// 		willClose: () => {
	// 			clearInterval(timerInterval);
	// 		},
	// 	});
	// };


	// Show the Swal alert for loading
	const showLoadingSwal = () => {
		Swal.fire({
			title: "Loading...",
			allowOutsideClick: false,
			didOpen: () => {
				Swal.showLoading();
			},
		});
	};

	// Close the Swal alert
	const closeLoadingSwal = () => {
		Swal.close();
	};

	// Effect to show and close Swal based on loading state
	useEffect(() => {
		if (isLoading) {
			showLoadingSwal();
		} else {
			closeLoadingSwal();
		}

		// Cleanup to ensure Swal closes if the component unmounts
		return () => {
			closeLoadingSwal();
		};
	}, [isLoading]);


	useEffect(() => {

		showLoadingSwal();
		
		fetch(`${process.env.REACT_APP_API_URL}/products`)
			.then((res) => res.json())
			.then((data) => {
				// console.log(data);
				setProducts(
					data.reverse().map((product) => {
						return (
							<Col xs={6} lg={3} className="mb-2 mb-lg-4" key={product._id}>
								<ProductCard
									id={product._id}
									name={product.name}
									price={product.price.toLocaleString()}
									src={product.src}
								/>
							</Col>
						);
					})
				);
				setIsLoading(false);
				closeLoadingSwal();
			});
	}, []);

	return (
		<Fragment>
			<section id="shop-all">
				<Container fluid className="min-vh-100 padding-x py-5">
					{user.isAdmin ? (
						<Fragment>
							<Breadcrumb className="px-lg-3">
								<Breadcrumb.Item>
									<Link to="/" className="text-dark">
										HOME
									</Link>
								</Breadcrumb.Item>

								<Breadcrumb.Item active>ALL ACTIVE PRODUCTS</Breadcrumb.Item>
							</Breadcrumb>
							<h1 className="big-heading px-lg-3 mt-4 mb-5">
								ALL ACTIVE PRODUCTS
							</h1>
						</Fragment>
					) : (
						<Fragment>
							<Breadcrumb className="px-lg-3">
								<Breadcrumb.Item>
									<Link to="/" className="text-dark">
										HOME
									</Link>
								</Breadcrumb.Item>

								<Breadcrumb.Item active>SHOP ALL</Breadcrumb.Item>
							</Breadcrumb>
							<h1 className="big-heading px-lg-3 mt-4 mb-5">SHOP ALL</h1>
						</Fragment>
					)}

					{isLoading ? (
						<p className="ms-lg-3">Loading...</p>
					) : (
						<Fragment>
							<Row className="px-lg-3">{products}</Row>
						</Fragment>
					)}
				</Container>
			</section>
		</Fragment>
	);
}
