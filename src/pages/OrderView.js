import { Fragment, useState, useEffect, useContext } from "react";
import { Link, Navigate } from "react-router-dom";
import { Button } from "react-bootstrap";
import UserContext from "./UserContext";

// import OrderCard from "../components/OrderCard";

export default function OrderView() {
	const { user } = useContext(UserContext);
	const [ordersList, setOrdersList] = useState("");

	// const [isLoading, setIsLoading] = useState(true);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/my-orders`, {
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				// console.log(data);
				// console.log(data[1].items[1].product[0].name);

				setOrdersList(
					data.reverse().map((order) => {
						let optionsDate = {
							year: "numeric",
							month: "long",
							day: "numeric",
						};
						console.log(order.purchasedOn);
						console.log(new Date().toLocaleDateString());

						let orderDate = new Date(order.purchasedOn).toLocaleDateString(
							"en-PH",
							optionsDate
						);
						// let orderDate = new Date().toLocaleDateString();

						// console.log(order.totalAmount);

						return (
							<Fragment key={order._id}>
								<div className="row justify-content-between">
									<div className="col-md-9">
										<div className="products ">
											{order.items.map((subItem, index) => {
												{
													/* console.log(subItem.product[0].name);
												console.log(subItem.product[0].productId); */
												}

												return (
													<Fragment key={index}>
														<div className="order-item mb-4">
															<div className="row">
																<div className="col-5 col-lg-3">
																	<Link
																		to={`/products/${subItem.product[0].productId}`}
																	>
																		<img
																			className="w-100 pl-3"
																			src={subItem.product[0].src}
																			alt=""
																		></img>
																	</Link>
																</div>
																<div className="col-7 col-lg-9">
																	<div>
																		<div className="text-success">
																			{order.status}
																		</div>
																	</div>
																	<div>
																		<div>
																			<Link
																				to={`/products/${subItem.product[0].productId}`}
																				className="link-dark text-decoration-none"
																			>
																				{subItem.product[0].name}
																			</Link>
																		</div>
																	</div>
																	<div>
																		<div className="fw-light">
																			₱
																			{subItem.product[0].price.toLocaleString()}
																		</div>
																	</div>
																	<div>
																		<div className="fw-light">
																			Qty: {subItem.product[0].quantity}
																		</div>
																	</div>
																	<div className="fw-light">
																		<div>Ordered: {orderDate}</div>
																	</div>
																	<div className="fw-light">
																		<div>Order #{order._id}</div>
																	</div>
																</div>
															</div>
														</div>
													</Fragment>
												);
											})}
										</div>
									</div>
									<div className="col-md-3 col-lg-2">
										<div className="mb-4">
											<div className="d-flex justify-content-between">
												<span>Total</span>
												<span>₱{order.totalAmount.toLocaleString()}</span>
											</div>
										</div>
										<Button
											as={Link}
											to="/shop-all"
											variant="dark"
											className="w-100 rounded-0"
										>
											SHOP AGAIN
										</Button>
									</div>
									<div className="p-15px my-4">
										<div className="border-top mb-4"></div>
									</div>
								</div>
							</Fragment>
						);
					})
				);
				// setIsLoading(false);
			});
	}, []);

	return user.isAdmin ||
		(user.id === null && localStorage.getItem("token") === null) ? (
		<Navigate to="/" />
	) : (
		<Fragment>
			<main className="min-vh-100 container-fluid padding-x">
				<div className="px-2">
					<div>
						<h1 className="big-heading text-center py-5">
							{ordersList.length > 0 ? "ORDERS" : "ORDER"}
						</h1>
					</div>
					{ordersList.length === 0 ? (
						<div>
							<h5 className="text-center mb-4">
								Your order is currently empty.
							</h5>

							<h5 className="text-center">
								<Link className="link-dark" to="/shop-all">
									Continue Shopping
								</Link>
							</h5>
						</div>
					) : (
						<Fragment>{ordersList}</Fragment>
					)}
				</div>
			</main>
		</Fragment>
	);
}
