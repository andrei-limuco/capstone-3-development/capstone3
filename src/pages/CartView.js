import { Fragment, useState, useEffect, useContext } from "react";
import UserContext from "./UserContext";
import { Link, useNavigate } from "react-router-dom";
import { Button } from "react-bootstrap";
import CartCard from "../components/CartCard";
import Swal from "sweetalert2";
import { Navigate } from "react-router-dom";

export default function CartView() {
	const { user, badgeCount, setBadgeCount } = useContext(UserContext);

	const navigate = useNavigate();

	const [carts, setCarts] = useState("");
	const [isLoading, setIsLoading] = useState(true);
	const [totalAmount, setTotalAmount] = useState(0);
	const [isEmpty, setIsEmpty] = useState(false);

	const cartTotalArray = [];
	const addSubTotals = (cartData) => {
		cartData.forEach((element) => {
			cartTotalArray.push(element.subTotal);
		});
		// console.log("cartTotalArray: " + cartTotalArray);
	};

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/carts`, {
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				addSubTotals(data);
				if (cartTotalArray.length !== 0) {
					let total = cartTotalArray.reduce(
						(previousSubtotal, currentSubtotal) => {
							return previousSubtotal + currentSubtotal;
						}
					);
					setTotalAmount(total);
				}

				setCarts(
					data.map((cart) => {
						for (let i = 0; i < cart.product.length; i++) {
							return (
								<CartCard
									key={cart._id}
									id={cart._id}
									productId={cart.product[i].productId}
									name={cart.product[i].name}
									price={cart.product[i].price}
									quantity={cart.product[i].quantity}
									src={cart.product[i].src}
									subtotal={cart.subTotal}
								/>
							);
						}
					})
				);
				setIsLoading(false);
			});
	}, [carts]);

	function deleteAll() {
		fetch(`${process.env.REACT_APP_API_URL}/carts/deleteAll`, {
			method: "DELETE",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
		});
	}

	const placeOrder = () => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
			body: JSON.stringify({
				totalAmount: totalAmount,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				console.log(data);

				if (data === true) {
					Swal.fire({
						title: "Checkout Successful",
						icon: "success",
						text: "Order Placed",
					});
					setBadgeCount(0);
					setIsEmpty(true);
					deleteAll();
					navigate("/orders");
				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please Try Again",
					});
				}
			});
	};

	return user.isAdmin ||
		(user.id === null && localStorage.getItem("token") === null) ? (
		<Navigate to="/" />
	) : (
		<Fragment>
			<main className="min-vh-100 container-fluid padding-x">
				<div className="px-2">
					<div>
						<h1 className="big-heading text-center py-5">CART({badgeCount})</h1>
					</div>
					{isLoading ? (
						"Loading..."
					) : (
						<Fragment>
							{isEmpty || carts.length === 0 ? (
								<div>
									<h5 className="text-center mb-4">
										Your cart is currently empty.
									</h5>

									<h5 className="text-center">
										<Link className="link-dark" to="/shop-all">
											Continue Shopping
										</Link>
									</h5>
								</div>
							) : (
								<div className="row justify-content-between">
									<div className="col-md-5 col-lg-4 order-md-2 mb-5">
										<h5 className="text-center mb-4">SUMMARY</h5>
										<div className="mb-4">
											{/* <div className="d-flex justify-content-between mb-2">
												<span>Subtotal</span>
												<span>₱{totalAmount.toLocaleString()}</span>
											</div> */}
											<div className="d-flex justify-content-between mb-2">
												<span>Shipping</span>
												<span>Free</span>
											</div>
											<div className="d-flex justify-content-between">
												<span>Total</span>
												<span>₱{totalAmount.toLocaleString()}</span>
											</div>
										</div>
										<Button
											onClick={placeOrder}
											variant="dark"
											className="w-100 rounded-0"
										>
											CHECKOUT
										</Button>
									</div>

									<div className="col-md-7">
										<div className="cart-items mb-5">{carts}</div>
									</div>
								</div>
							)}
						</Fragment>
					)}
				</div>
			</main>
		</Fragment>
	);
}
