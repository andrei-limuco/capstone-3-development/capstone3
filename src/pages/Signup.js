import { useState, useEffect, useContext } from "react";
import UserContext from "./UserContext";
import Swal from "sweetalert2";
import { Navigate, useNavigate } from "react-router-dom";

export default function Signup() {
	const navigate = useNavigate();

	const { user } = useContext(UserContext);
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isActive, setIsActive] = useState(false);

	const registerUser = (e) => {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email: email,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				console.log(data);
				if (data === true) {
					Swal.fire({
						title: "Duplicate email found",
						icon: "error",
						text: "Please provide a different email",
					});
				} else {
					fetch("https://fierce-coast-55539.herokuapp.com/users/register", {
						method: "POST",
						headers: {
							"Content-Type": "application/json",
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							password: password1,
						}),
					});

					Swal.fire({
						title: "Registration Successful",
						icon: "success",
					});

					setFirstName("");
					setLastName("");
					setEmail("");
					setPassword1("");
					setPassword2("");

					navigate("/login");
				}
			});
	};

	useEffect(() => {
		if (
			firstName !== "" &&
			lastName !== "" &&
			email !== "" &&
			password1 !== "" &&
			password2 !== "" &&
			password1 === password2
		) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, email, password1, password2]);

	return user.id !== null ? (
		<Navigate to="/" />
	) : (
		<main className="min-vh-100 form-signup text-center my-5 my-md-0">
			<form onSubmit={(e) => registerUser(e)}>
				{/* <img
					className="mb-4"
					src="/images/logo.png"
					alt=""
					width="72"
					height="72"
				/> */}
				<h1 className="mb-3 fw-normal big-heading">CREATE ACCOUNT</h1>

				<div className="form-floating">
					<input
						onChange={(e) => setFirstName(e.target.value)}
						value={firstName}
						type="text"
						className="form-control"
						id="floatingInput"
						placeholder="First Name"
					/>
					<label htmlFor="floatingInput">First Name</label>
				</div>
				<div className="form-floating last-name">
					<input
						onChange={(e) => setLastName(e.target.value)}
						value={lastName}
						type="text"
						className="form-control"
						id="floatingInput"
						placeholder="Last Name"
					/>
					<label htmlFor="floatingInput">Last Name</label>
				</div>
				<div className="form-floating">
					<input
						onChange={(e) => setEmail(e.target.value)}
						value={email}
						type="email"
						className="form-control"
						id="floatingInput"
						placeholder="name@example.com"
					/>
					<label htmlFor="floatingInput">Email address</label>
				</div>
				<div className="form-floating">
					<input
						onChange={(e) => setPassword1(e.target.value)}
						value={password1}
						type="password"
						className="form-control"
						id="floatingPassword"
						placeholder="Password1"
					/>
					<label htmlFor="floatingPassword">Password</label>
				</div>
				<div className="form-floating">
					<input
						onChange={(e) => setPassword2(e.target.value)}
						value={password2}
						type="password"
						className="form-control"
						id="floatingPassword"
						placeholder="Password2"
					/>
					<label htmlFor="floatingPassword">Verify Password</label>
				</div>
				<div className="checkbox mb-3"></div>
				{isActive ? (
					<button className="w-100 btn btn-lg btn-dark rounded-0" type="submit">
						SIGN UP
					</button>
				) : (
					<button
						className="w-100 btn btn-lg btn-dark rounded-0 disabled"
						type="submit"
					>
						SIGN UP
					</button>
				)}

				{/* <p className="mt-5 mb-3 text-muted">&copy; 2017–2021</p> */}
			</form>
		</main>
	);
}
