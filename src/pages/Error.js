import { Link } from "react-router-dom";

export default function Error() {
	return (
		<section className="min-vh-100 row justify-content-center py-5 text-center">
			<div className="col-md-6 error-page">
				<h1 className="mb-5 big-heading">PAGE NOT FOUND</h1>

				<h5 className="mb-5">
					We can't find the page you are looking for. Sorry for the
					inconvenience.
				</h5>
				<h5 className="mb-5">
					<Link className="link-dark" to="/">
						Return to homepage
					</Link>
				</h5>
			</div>
		</section>
	);
}
