import { Fragment, useContext } from "react";

import Carousels from "../components/Carousels";
import Trending from "../components/Trending";
import Collections from "../components/Collections";
import BestSellers from "../components/BestSellers";
import AdminDashboard from "./AdminDashboard";

import UserContext from "./UserContext";

export default function Home() {
	const { user } = useContext(UserContext);

	return !user.isAdmin ? (
		<Fragment>
			<Carousels />
			<Trending />
			<Collections />
			<BestSellers />
		</Fragment>
	) : (
		<AdminDashboard />
	);
}
