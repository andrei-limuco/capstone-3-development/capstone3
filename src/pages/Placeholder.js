// import { Card, Button, Placeholder } from "react-bootstrap";

// export default function PlaceholderTest() {
// 	return (
// 		<div className="d-flex justify-content-around">
// 			<Card style={{ width: "18rem" }}>
// 				<Card.Img variant="top" src="holder.js/100px180" />
// 				<Card.Body>
// 					<Card.Title>Card Title</Card.Title>
// 					<Card.Text>
// 						Some quick example text to build on the card title and make up the
// 						bulk of the card's content.
// 					</Card.Text>
// 					<Button variant="primary">Go somewhere</Button>
// 				</Card.Body>
// 			</Card>

// 			<Card style={{ width: "18rem" }}>
// 				<Card.Img variant="top" src="holder.js/100px180" />
// 				<Card.Body>
// 					<Placeholder as={Card.Title} animation="glow">
// 						<Placeholder xs={6} />
// 					</Placeholder>
// 					<Placeholder as={Card.Text} animation="glow">
// 						<Placeholder xs={7} /> <Placeholder xs={4} /> <Placeholder xs={4} />{" "}
// 						<Placeholder xs={6} /> <Placeholder xs={8} />
// 					</Placeholder>
// 					<Placeholder.Button variant="primary" xs={6} />
// 				</Card.Body>
// 			</Card>
// 		</div>
// 	);
// }

import React from "react";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Badge from "@material-ui/core/Badge";
// import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import * as BsIcons from "react-icons/bs";

export default function App() {
	const [itemCount, setItemCount] = React.useState(1);

	return (
		<div style={{ display: "block", padding: 30 }}>
			<h4>How to create ShoppingCart Button in ReactJS?</h4>
			<div>
				<Badge color="secondary" badgeContent={itemCount}>
					<BsIcons.BsCart4 size={25} />{" "}
				</Badge>
				<ButtonGroup>
					<Button
						onClick={() => {
							setItemCount(Math.max(itemCount - 1, 0));
						}}
					>
						{" "}
						<RemoveIcon fontSize="small" />
					</Button>
					<Button
						onClick={() => {
							setItemCount(itemCount + 1);
						}}
					>
						{" "}
						<AddIcon fontSize="small" />
					</Button>
				</ButtonGroup>
			</div>
		</div>
	);
}
