import { Fragment, useState, useEffect, useContext } from "react";
import { useParams, Link } from "react-router-dom";
import UserContext from "./UserContext";
import { Container, Row, Col, Breadcrumb, Button } from "react-bootstrap";
import Swal from "sweetalert2";

export default function ProductView() {
	const { user, setBadgeCount } = useContext(UserContext);

	// const navigate = useNavigate();

	const { productId } = useParams();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [image, setImage] = useState("");
	const [quantity, setQuantity] = useState(1);

	const decrease = () => {
		if (quantity > 1) {
			setQuantity(quantity - 1);
		}
	};

	const increase = () => {
		setQuantity(quantity + 1);
	};

	const addToCart = (productId) => {
		fetch(
			`${process.env.REACT_APP_API_URL}/carts/${productId}/addToCart`,
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${localStorage.getItem("token")}`,
				},
				body: JSON.stringify({
					quantity: quantity,
				}),
			}
		)
			.then((res) => res.json())
			.then((data) => {
				// console.log(data);
				if (data === true) {
					setBadgeCount((prevCount) => prevCount + 1);
					Swal.fire({
						title: "Successfully Added to Cart!",
						icon: "success",
					});
					setQuantity(1);
				} else {
					Swal.fire({
						title: "Something Went Wrong",
						icon: "error",
						text: "Please Try Again",
					});
				}
			});
	};

	useEffect(() => {
		//console.log(productId)
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
			.then((res) => res.json())
			.then((data) => {
				// console.log(data)

				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
				setImage(data.src);
			});
	}, [productId]);

	return (
		<Fragment>
			<section>
				<Container fluid className="product-padding-x py-5">
					<Breadcrumb className="mb-5">
						<Breadcrumb.Item>
							<Link to="/" className="text-dark">
								HOME
							</Link>
						</Breadcrumb.Item>
						<Breadcrumb.Item>
							<Link to="/shop-all" className="text-dark">
								PRODUCTS
							</Link>
						</Breadcrumb.Item>
						<Breadcrumb.Item active>{name.toUpperCase()}</Breadcrumb.Item>
					</Breadcrumb>
					<Row>
						<Col md={6}>
							<img className="w-100 mb-3" src={image} alt="" />
						</Col>
						<Col md={6}>
							<h2>{name}</h2>
							<h5 className="mb-4 text-secondary">₱{price.toLocaleString()}</h5>
							<p>Quantity:</p>
							<div className="d-flex justify-content-center align-items-center mb-4">
								<Button
									onClick={decrease}
									variant="outline-dark"
									className="rounded-0"
								>
									-
								</Button>
								<span className="quantity">{quantity}</span>
								<Button
									onClick={increase}
									variant="outline-dark"
									className="rounded-0"
								>
									+
								</Button>

								{!user.isAdmin ? (
									user.id !== null ? (
										<Button
											onClick={() => addToCart(productId)}
											type="submit"
											variant="dark"
											className="rounded-0 w-100 ms-3"
										>
											ADD TO CART
										</Button>
									) : (
										<Link
											className="btn btn-danger rounded-0 w-100 ms-3"
											to="/login"
										>
											LOG IN TO ORDER
										</Link>
									)
								) : (
									<Link
										className="btn btn-dark rounded-0 w-100 ms-3 disabled"
										to="/login"
									>
										ADD TO CART
									</Link>
								)}
							</div>
							<p className="lead">{description}</p>
						</Col>
					</Row>
				</Container>
			</section>
		</Fragment>
	);
}
